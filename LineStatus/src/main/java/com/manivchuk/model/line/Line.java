package com.manivchuk.model.line;

import java.net.InetAddress;

/**
 * Created by ASUS on 22.12.2016.
 */
public class Line {
    private int id;
    private String name;
    private boolean status = false;
    private InetAddress mcastAddr;
    private int port;

    public Line() {
    }

    public Line(int id, String name, InetAddress mcastAddr, int port) {
        this.id = id;
        this.name = name;
        this.mcastAddr = mcastAddr;
        this.port = port;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public InetAddress getMcastAddr() {
        return mcastAddr;
    }

    public void setMcastAddr(InetAddress mcastAddr) {
        this.mcastAddr = mcastAddr;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}

