package com.manivchuk.model.line;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by ASUS on 22.12.2016.
 */
public class QueueLine {
    private static ConcurrentLinkedQueue<Line> queue = new ConcurrentLinkedQueue<>();

    public static void addLine(Line line){
        queue.add(line);
        System.out.println("Line was add.");
    }
    public static Line getLine(){
        System.out.println("Line was get.");
        return queue.poll();
    }
    public static int size(){
        return queue.size();
    }

}
