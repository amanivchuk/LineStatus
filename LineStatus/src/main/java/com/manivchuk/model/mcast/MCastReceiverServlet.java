package com.manivchuk.model.mcast;


import com.manivchuk.model.line.Line;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by ASUS on 20.12.2016.
 */

public class MCastReceiverServlet extends HttpServlet {
    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {

    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

        int mcastPortMSSR_Osn = 6000;
        InetAddress mcastAddrMSSR_Osn = null;

        int mcastPortMSSR_Rez = 6005;
        InetAddress mcastAddrMSSR_Rez = null;


        try{
            mcastAddrMSSR_Osn = InetAddress.getByName("230.0.0.1");
            mcastAddrMSSR_Rez = InetAddress.getByName("230.1.0.5");

        } catch (UnknownHostException e) {
            System.out.println("=> Problems getting the symbolic multicast address");
            e.printStackTrace(); System.exit(1);
        }
        Line line_MSSR_osn = new Line(1, "Mssr osn.", mcastAddrMSSR_Osn,mcastPortMSSR_Osn);
        Line line_MSSR_rez = new Line(1, "Mssr rez.", mcastAddrMSSR_Rez,mcastPortMSSR_Rez);

        //start new thread to receive multicasts
        Thread line_MSSRosn = new Thread(new McastReceiver(line_MSSR_osn), "McastReceiver");
        line_MSSRosn.setName("MSSR osn.");
        line_MSSRosn.start();

        new Thread(new McastRepeater(line_MSSR_osn), "McastRepeater").start();
        try {
            Thread.sleep(8000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //start new threaf to send multicast
        Thread line_MSSRrez = new Thread(new McastReceiver(line_MSSR_rez), "McastReceiver");
        line_MSSRrez.setName("MSSR rez.");
        line_MSSRrez.start();

        new Thread(new McastRepeater(line_MSSR_rez), "McastRepeater").start();
    }
}
