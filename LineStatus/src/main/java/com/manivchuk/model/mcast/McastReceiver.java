package com.manivchuk.model.mcast;

import com.manivchuk.model.line.Line;
import com.manivchuk.model.line.QueueLine;
import com.manivchuk.model.pack.Packet;
import com.manivchuk.model.pack.QueuePacket;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.*;
import java.util.Date;

/**
 * Created by ASUS on 20.12.2016.
 */
public class McastReceiver implements Runnable{
    private int mcastPort = 0;
    private InetAddress mcastAddr = null;
    private InetAddress localHost = null;
    private Line line;

    public McastReceiver(Line line) {
        this.line = line;
        this.mcastPort = line.getPort();
        this.mcastAddr = line.getMcastAddr();
        try{
            localHost = InetAddress.getLocalHost();
            System.out.println("localHost => " + localHost);
            System.out.println("mcastAddr = > "+ mcastAddr);
        } catch (UnknownHostException e) {
            System.out.println("=> Problems identifying localhost.");
            e.printStackTrace(); System.exit(1);
        }
    }

    @Override
    public void run() {
        MulticastSocket mSocket = null;
        try{
            System.out.println("Setting up multicast receiver");
            mSocket = new MulticastSocket(mcastPort);
            mSocket.joinGroup(mcastAddr);
        } catch (IOException e) {
            System.out.println("=> Trouble opening multicast port");
            e.printStackTrace(); System.exit(1);
        }

        DatagramPacket packet;
        System.out.println("Multicast receiver set up");
        while(true){
            try{
                byte[] buf = new byte[1024];
                packet = new DatagramPacket(buf, buf.length);
                System.out.println(line.getId()+" " + line.getName()+": waiting for packet");
                mSocket.setSoTimeout(8000);
                mSocket.receive(packet);

                System.out.println(line.getId()+" " + line.getName()+": received pack => " + new Date());

                ByteArrayInputStream bistream = new ByteArrayInputStream(packet.getData());
                ObjectInputStream ois = new ObjectInputStream(bistream);
                Integer value = (Integer) ois.readObject();

                /*System.out.println("====> Values = " + value);
                System.out.println("InetAddr => "+packet.getAddress());
                System.out.println("localHost => "+localHost);
                */
                Packet packets = new Packet();
                packets.setSocketAddress(packet.getSocketAddress());
                packets.setInetAddress(packet.getAddress());
                packets.setPort(packet.getPort());
                packets.setData(packet.getData());
                packets.setLength(packet.getLength());
                line.setStatus(true);

                QueuePacket.addPacket(packets);
                QueueLine.addLine(line);

                //Ignore packets from myself, print the rest
                if(!(packet.getAddress().equals(localHost))){
                    System.out.println("Received multicast packet: " + value.intValue() + " from: " + packet.getAddress());
                }
                ois.close();
                bistream.close();
            }
            catch (SocketTimeoutException timeOut){
                System.out.println(line.getId()+" " + line.getName()+"==============> Line failed!!!");
                McastRepeater.timeSleep = 1;

                Packet packets = new Packet();
                line.setName(Thread.currentThread().getName());
                line.setStatus(false);

                QueuePacket.addPacket(packets);
                QueueLine.addLine(line);
            }
            catch (ClassNotFoundException e) {
                System.out.println("=> Class missing while reading mcast packet");
                e.printStackTrace();
            }catch (IOException e) {
                System.out.println("=> Trouble reading multicast message");
                e.printStackTrace(); System.exit(1);
            }
        }
    }
}
