package com.manivchuk.model.mcast;

import com.manivchuk.model.line.Line;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by ASUS on 20.12.2016.
 */
public class McastRepeater implements Runnable{
    public static int timeSleep = 1;
    private DatagramSocket datagramSocket = null;
    int mcastPort = 0;
    InetAddress mcastAddr = null;
    InetAddress localHost = null;
    Line line = null;

    public McastRepeater(Line line) {
        this.line = line;
        this.mcastPort = line.getPort();
        this.mcastAddr = line.getMcastAddr();
        try{
            datagramSocket = new DatagramSocket();
        } catch (SocketException e) {
            System.out.println("=> Problems creating the datagram socket.");
            e.printStackTrace(); System.exit(1);
        }
        try{
            localHost = InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            System.out.println("=> Problems identifying localhost.");
            e.printStackTrace(); System.exit(1);
        }
    }

    @Override
    public void run() {
        DatagramPacket packet = null;
        int count = 0;

        //send multicast msg once per second
        while(true){
            //create the packet to send
            try{
                //serialize the multicast message
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bos);
                out.writeObject(count++);
                out.flush();
                out.close();

                //Create a datagram packet and send it
                packet = new DatagramPacket(bos.toByteArray(), bos.size(), mcastAddr, mcastPort);

                //send the packet
                datagramSocket.send(packet);
                System.out.println(line.getId()+" " + line.getName()+ " sending multicast message.");
                TimeUnit.SECONDS.sleep(timeSleep++);

            } catch (InterruptedException e) {
                System.out.println("=> error sending multicast");
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}