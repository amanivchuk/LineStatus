package com.manivchuk.model.pack;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by ASUS on 20.12.2016.
 */
public class QueuePacket {
    private static ConcurrentLinkedQueue<Packet> queue = new ConcurrentLinkedQueue<>();

    public static void addPacket(Packet packet){
        queue.add(packet);
        System.out.println("Packet was add.");
    }
    public static Packet getPacket(){
        System.out.println("Packet was get.");
        return queue.poll();
    }
    public static int size(){
        return queue.size();
    }

}
