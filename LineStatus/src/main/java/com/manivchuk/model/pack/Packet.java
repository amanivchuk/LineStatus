package com.manivchuk.model.pack;

import java.net.InetAddress;
import java.net.SocketAddress;
import java.util.Iterator;

/**
 * Created by ASUS on 20.12.2016.
 */
public class Packet{
    private SocketAddress socketAddress;
    private InetAddress inetAddress;
    private int port;
    private byte[] data;
    private int length;

    public Packet() {
    }

    public Packet(SocketAddress socketAddress, InetAddress inetAddress, int port, byte[] data, int length) {
        this.socketAddress = socketAddress;
        this.inetAddress = inetAddress;
        this.port = port;
        this.data = data;
        this.length = length;
    }

    public SocketAddress getSocketAddress() {
        return socketAddress;
    }

    public void setSocketAddress(SocketAddress socketAddress) {
        this.socketAddress = socketAddress;
    }

    public InetAddress getInetAddress() {
        return inetAddress;
    }

    public void setInetAddress(InetAddress inetAddress) {
        this.inetAddress = inetAddress;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}
