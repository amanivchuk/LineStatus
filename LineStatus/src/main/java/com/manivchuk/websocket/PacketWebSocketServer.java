package com.manivchuk.websocket;

//import com.google.gson.Gson;
import com.manivchuk.model.line.Line;
import com.manivchuk.model.line.QueueLine;
import com.manivchuk.model.pack.QueuePacket;
import org.json.JSONObject;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by ASUS on 20.12.2016.
 */
@ServerEndpoint("/websocket")
public class PacketWebSocketServer {

    private final Set<Session> sessions = new HashSet<>();

    //@Inject
    //private LineSessionHandler sessionHandler;

    @OnMessage
    public void onMessage(String message, Session session) throws IOException, InterruptedException {

        System.out.println("User input: " + message);
        session.getBasicRemote().sendText("Hello. " + message);
sessions.add(session);

        while (true){
            //session.getBasicRemote().sendText("=========");
            //sessionHandler.getInformation();
            if(QueueLine.size() != 0) {
                JSONObject json = new JSONObject();
                Line line = QueueLine.getLine();
                json.put("id", line.getId());
                json.put("name", line.getName());
                json.put("status", line.isStatus());

                System.out.println("======GSON========>>> " + json.toString());
                session.getBasicRemote().sendText(json.toString());
                //sendToAllConnection(gson);
            }else {
                Thread.sleep(1000);
            }

/*
               if(QueuePacket.size() != 0){
                   Packet packet = QueuePacket.getPacket();
                   Line line = QueueLine.getLine();
//                   String socketAddress = "0.0.0.0";
//                    if(packet.getSocketAddress() != null){
//                    socketAddress = String.valueOf(packet.getSocketAddress());
//                    }
                   if(line.isStatus()) {
                       byte[] b = packet.getData();
                       session.getBasicRemote().sendText(line.getId() + " " + line.getName() + " " + packet.getSocketAddress() + " => " + new BigInteger(b).intValue() + " status = " + line.isStatus());
                   }
                   if(!line.isStatus()){
                       session.getBasicRemote().sendText(line.getId() + " " + line.getName() + " " + " => " + " status = " + line.isStatus());
                   }
               }
            else {
                   Thread.sleep(1000);
               }*/
        }
    }

    @OnOpen
    public void onOpen(Session session) {
        System.out.println("Client connected");
        System.out.println("------------------Session = " + session);
        //sessionHandler.addSession(session);
    }

    @OnClose
    public void close(Session session) {
        //sessionHandler.removeSession(session);
    }
    @OnError
    public void onError(Throwable error) {
        System.out.println("Error!!!!!!!!!!");
    }

    //-----------------------------------------------------------------------------------------------------------------

    /*private void sendToAllConnection(Gson gson) {
        for(Session session : sessions){
            sendToSession(session, gson);
        }
    }

    private void sendToSession(Session session, Gson gson) {
        try {
            session.getBasicRemote().sendText(gson.toString());
            //session.getBasicRemote().sendText("geson");
        } catch (IOException e) {
            System.out.println("Ooops, problem sending to session.");
            e.printStackTrace();
        }
    }*/

}
