package com.manivchuk.websocket;

//import com.google.gson.Gson;
import com.manivchuk.model.line.Line;
import com.manivchuk.model.line.QueueLine;
import com.manivchuk.model.pack.Packet;
import com.manivchuk.model.pack.QueuePacket;

import javax.json.JsonObject;
import javax.websocket.Session;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by ASUS on 23.12.2016.
 */
public class LineSessionHandler {
    private final Set<Session> sessions = new HashSet<>();

    public void addSession(Session session) {
        sessions.add(session);
    }

    public void getInformation(){

        //Gson gson = new Gson();
       // String pack = gson.toJson(QueuePacket.getPacket());
        //String lin = gson.toJson(QueueLine.getLine());
        System.out.println("!!!!!!!!!!!!!--------------------------------------->>> " /*+ pack*/);
        System.out.println("!!!!!!!!!!!!!--------------------------------------->>> " /*+ lin*/);

        //sendToAllConnection(gson);
    }

    private void sendToAllConnection(JsonObject gson) {
        for(Session session : sessions){
            //sendToSession(session, gson);
        }
    }

    private void sendToSession(Session session, JsonObject gson) {
        try {
            session.getBasicRemote().sendText(gson.toString());
        } catch (IOException e) {
            System.out.println("Ooops, problem sending to session.");
            e.printStackTrace();
        }
    }

    public void removeSession(Session session) {
        sessions.remove(session);
    }
}
